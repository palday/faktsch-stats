# Makefile for the faktsch series of experiments (statistics)
#
# Copyright (c) 2014 Phillip Alday
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# try doing things in parallel with the options --jobs=n and --max-load=n+1
# where you replace n with the number of processors/cores. that will perform that
# many jobs in parallel, but the max-load option limits spawning new processes
# when the system is under load the load time has risen unacceptably

prefix ?= fa
exploration_models ?= formedness.training.cz.int.RData existence.training.cz.int.RData
data ?=  fa_bp016_30_500r_lme
exploration_data = $(data)_exploration.tab.xz
confirmation_data = $(data)_confirmation.tab.xz

# block deletion of intermediates --
# esp since Make misidentifies our actual desired targets as intermediates
.SECONDARY:

clean:
	rm faktsch_exploration.RData
	rm faktsch_confirmation.RData
	touch $(data)

faktsch_exploration.RData: preproc_training.R preproc.R $(exploration_data)
	R --vanilla --slave < preproc_training.R

$(exploration_models): explore.R faktsch_exploration.RData
	R --vanilla --slave < explore.R

explore: windows

windows: windows.Rout

windows.Rout : $(exploration_models) windows.R
	R --vanilla --slave < windows.R

faktsch_confirmation.RData: preproc_training.R preproc.R $(confirmation_data)
	R --vanilla --slave < preproc_test.R

stats: report.R poa.R faktsch_confirmation.RData
	R --vanilla --slave < report.R
