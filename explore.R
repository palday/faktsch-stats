# Copyright (c) 2014 Phillip Alday
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

library(lme4)

if(!exists("faktsch.exploration")){
  load("faktsch_exploration.RData")
} 

formedness.training.cz.int <- lmer(mean ~ win + win:formedness + (1|subj) + (1|item)
                                   ,data=subset(faktsch.exploration,phase=="training" & chan=="Cz"))

save(formedness.training.cz.int,file="formedness.training.cz.int.RData")
rm(formedness.training.cz.int)

existence.training.cz.int <- lmer(mean ~ win + win:existence + (1|subj) + (1|item)
                                  ,data=subset(faktsch.exploration,phase=="training" & chan=="Cz"))

save(existence.training.cz.int,file="existence.training.cz.int.RData")
rm(existence.training.cz.int)
# faktsch <- read.table(xzfile("po_lme_many.tab.xz")
#                       ,header=TRUE,
#                       ,colClasses=c("character")
# )
# source("load_data.R")
# faktsch <- subset(faktsch, !(subj %in% c("3","19","11")) )
# save(faktsch, file="po_many.RData", compress="xz")
# 
# # load("po_many.RData")
# library(lme4)
# formedness.training <- lmer(mean ~ win*formedness + (1|subj) + (1|item), data=subset(faktsch,phase=="training"))
# save(formedness.training,file="po.formedness.training.RData")
# rm(formedness.training)
# 
# # load("po_many.RData")
# library(lme4)
# existence.training <- lmer(mean ~ win*existence + (1|subj) + (1|item), data=subset(faktsch,phase=="training"))
# save(existence.training,file="po.existence.training.RData")
# rm(existence.training)
# 
